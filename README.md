README

Airports assesment created by Victor Ochoa. 
The solution proposed uses Spring Boot and Elasticsearch to implement all the requeriments. The application includes a land page, called index.html from where it's possible to access all queries and reports graphically. Also it's possible to use the application as a REST API:

1. /query

	This endpoint returns all airports and runways ids for a given country name or code.
	
	Params: 
	
	searchTerm: Country or country code to be search

2. /reports/mostAirports

	This endpoint returns the countries with the most airports.
	
	Params: 
	
	size: Number of countries to be returned.

3. /reports/lowestAirports
	
	This endpoint returns the countries with the fewer airports.
	
	Params: 
	
	size: Number of countries to be returned.

4. /reports/surfaces
	
	This endpoint returns all countries and the surfaces associated to every airport of that country.

5. /reports/runwaysids
	
	This endpoint returns the most used ids for the runways.
	
	Params: 
	
	size: Number of runways to be returned.

Version: 1.0

HOW TO START

1. Clone "airport" repository (This repository).
	
2. Download elasticsearch.zip from Downloads section (https://bitbucket.org/kanito8a/airport/downloads/elasticsearch.zip).

3. Unzip elasticsearch.zip.

4. Run [EXTRACT_PATH]/elasticsearch/bin/elasticsearch (elasticsearch.bat for Windows)	

5. Once ElasticSearch is up and running, execute lunatech.airport.App to run the application.

6. Access http://localhost:8080/index.html.

CONTACT

kanito8a@gmail.com
