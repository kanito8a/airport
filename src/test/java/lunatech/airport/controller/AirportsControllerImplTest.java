package lunatech.airport.controller;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import techdata.airport.facade.ElasticSearchFacade;
import techdata.airport.rest.controller.AirportsControllerImpl;

/**
 * Unit test for simple App.
 */
@RunWith(MockitoJUnitRunner.class)
public class AirportsControllerImplTest
{
	@Autowired
	@InjectMocks
	AirportsControllerImpl aiportsController;

	static ElasticSearchFacade elasticSearchFacade = Mockito.mock(ElasticSearchFacade.class);

	/**
	 * This method creates the mock objects and the rules to be used in the tests
	 * @throws IOException
	 */
	@BeforeClass
	public static void testSetup() throws IOException {

		// ElasticSearchFacade mock object rules
		Mockito.when(elasticSearchFacade.getQuery(Mockito.anyString())).thenReturn("{}");
		Mockito.when(elasticSearchFacade.getQuery("")).thenReturn("");
		Mockito.when(elasticSearchFacade.getReportAirports(Mockito.anyInt(), Mockito.any())).thenReturn("{}");
		Mockito.when(elasticSearchFacade.getReportLowestAirports(Mockito.anyInt())).thenReturn("{}");
		Mockito.when(elasticSearchFacade.getReportMostAirports(Mockito.anyInt())).thenReturn("{}");
		Mockito.when(elasticSearchFacade.getReportSurfaces()).thenReturn("{}");
		Mockito.when(elasticSearchFacade.getReportRunwaysIds(Mockito.anyInt())).thenReturn("{}");
	}

	/**
	 * Test for /query endpoint merhod
	 */
	@Test
	public void testQuery()	{
		String testResult = aiportsController.query("test");
		assertEquals(testResult.equalsIgnoreCase("{}"), true);
	}

	/**
	 * Test for /reports/mostAirports endpoint method
	 */
	@Test
	public void testReport1() {
		String testResult = aiportsController.reportsMostAirports(10);
		assertEquals( testResult.equalsIgnoreCase("{}"), true );
	}

	/**
	 * Test for /reports/reportsLowestAirports endpoint method
	 */
	@Test
	public void testReport2() {
		String testResult = aiportsController.reportsLowestAirports(10);
		assertEquals( testResult.equalsIgnoreCase("{}"), true );
	}

	/**
	 * Test for /reports/runwaysids endpoint method
	 */
	@Test
	public void testReport3() {
		String testResult = aiportsController.reportsRunwaysIds(10);
		assertEquals( testResult.equalsIgnoreCase("{}"), true );
	}

	/**
	 * Test for /reports/surfaces endpoint method
	 */
	@Test
	public void testReport4() {
		String testResult = aiportsController.reportsSurfaces();
		assertEquals( testResult.equalsIgnoreCase("{}"), true );
	}

}
