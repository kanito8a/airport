package techdata.airport.rest.controller;

import java.io.IOException;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class defines the resources supported by the Service.
 * @author OchoaV
 *
 */
@RestController
@EnableAutoConfiguration
public interface AirportsController {

	/**
	 * This method implements "query" endpoint.
	 * This endpoint returns countries that match a searchTerm including all airports.
	 * and runways ids associated to them.
	 * @param searchTerm Term to search on ElasticSearch. This value will be
	 * a country name or country id.
	 * @return JSON document with the results of the query.
	 * @throws IOException
	 */
	@RequestMapping(value = "/query", method = RequestMethod.GET)
	@ResponseBody	
	String query(@RequestParam("searchTerm") String searchTerm);

	/**
	 * This method implements "reports/mostAirports" endpoint.
	 * This endpoint returns the airports with the highest number of airports attached to them.
	 * @return JSON document with the results of the query.
	 * @param size Number of airports to be returned.
	 * @throws IOException
	 */
	//@RequestMapping("/reports/mostAirports")
	@RequestMapping(value = "/reports/mostAirports", method = RequestMethod.GET)
	@ResponseBody
	String reportsMostAirports(@RequestParam("size") int size);

	/**
	 * This method implements "reports/lowestAirports" endpoint.
	 * This endpoint returns the airports with the fewer number of airports attached to them.
	 * @return JSON document with the results of the query.
	 * @param size Number of airports to be returned.
	 * @throws IOException
	 */	
	@RequestMapping(value = "/reports/lowestAirports", method = RequestMethod.GET)
	@ResponseBody
	String reportsLowestAirports(@RequestParam("size") int size);

	/**
	 * This method implements "reports/surfaces" endpoint.
	 * This endpoint returns the surfaces of all airports.
	 * @return JSON document with the results of the query.
	 * @throws IOException
	 */	
	@RequestMapping(value = "/reports/surfaces", method = RequestMethod.GET)
	@ResponseBody
	String reportsSurfaces();

	/**
	 * This method implements "reports/runwaysids" endpoint.
	 * This endpoint returns the runway le_idents grouped by total number.
	 * @param size Number of airports to be returned.
	 * @return JSON document with the results of the query.
	 * @throws IOException
	 */	
	@RequestMapping(value = "/reports/runwaysids", method = RequestMethod.GET)
	@ResponseBody
	String reportsRunwaysIds(@RequestParam("size") int size);

}
