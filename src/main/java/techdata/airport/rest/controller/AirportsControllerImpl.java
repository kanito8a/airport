package techdata.airport.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RestController;

import techdata.airport.facade.ElasticSearchFacade;

/**
 * This class implements the resources supported by the Service.
 * @author OchoaV
 *
 */
@RestController
@EnableAutoConfiguration
public class AirportsControllerImpl implements AirportsController{

	/**
	 * This facade object implements the connection with ElasticSearch.
	 */
	@Autowired
	private ElasticSearchFacade elasticSearchFacade;

	//
	// Public Methods
	//

	@Override
	public	String query(String searchTerm) {
		return elasticSearchFacade.getQuery(searchTerm);
	}

	@Override
	public String reportsMostAirports(int size) {
		return elasticSearchFacade.getReportMostAirports(size);
	}

	@Override
	public String reportsLowestAirports(int size) {
		return elasticSearchFacade.getReportLowestAirports(size);
	}

	@Override
	public String reportsSurfaces() {
		return elasticSearchFacade.getReportSurfaces();
	}

	@Override
	public String reportsRunwaysIds(int size) {
		return elasticSearchFacade.getReportRunwaysIds(size);
	}

}
