package techdata.airport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is the main class for the program. SpringApplication is started from the main method.
 * @author OchoaV
 *
 */
@SpringBootApplication
public class App {

	/**
	 * Main method. Starts Spring boot.
	 * @param args Application parameters.
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		SpringApplication.run(App.class, args);
	}
}
