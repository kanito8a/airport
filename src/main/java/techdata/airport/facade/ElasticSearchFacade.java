package techdata.airport.facade;


import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchPhrasePrefixQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.InnerHitBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.join.query.JoinQueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.jayway.jsonpath.JsonPath;

/**
 * This class exposes a series of methods that can be used to search in ElasticSearch.
 * @author OchoaV
 *
 */
@PropertySource("classpath:elastic.properties")
@Component
public class ElasticSearchFacade {

	/**
	 * Name of the ElasticSearch index. This value is used to configure TransportClient.
	 */
	@Value("${ES_INDEX_NAME}")
	private String ES_INDEX_NAME;

	/**
	 * Elastic search host name. This value is used to configure TransportClient.
	 */
	@Value("${ES_HOST_NAME}")
	private String ES_HOST_NAME;

	/**
	 * Port used to connect with elasticSearch. This value is used to configure TransportClient.
	 */
	@Value("${ES_HOST_PORT}")
	private int ES_HOST_PORT;

	/**
	 * Max number of children in a response. This value is used to return all children/values
	 * in a query.
	 */
	@Value("${ES_MAX_CHILDREN}")
	private int ES_MAX_CHILDREN;

	/**
	 * Transport client. This object will execute the queries against ElasticSearch.
	 */
	private TransportClient client = null;

	//
	// Public methods
	//

	/**
	 * Implements a generic query against countries, returning all airports and the
	 * runways ids attached to that airport.
	 * @param searchTerm Term to be search. This value can be a country name or a country ID.
	 * @return JSON with the results of the query.
	 * @throws IOException
	 */
	public String getQuery(final String searchTerm) {

		// This query:
		// Retrieves all countries that match the searchTerm both
		// in the "name" and "code" fields. Along with the country
		// all airports and runways are returned.
		QueryBuilder esQuery = boolQuery()
				.must(boolQuery()
						.should(matchPhrasePrefixQuery("name", searchTerm))
						.should(matchQuery("code", searchTerm)))
				.must(boolQuery()
						.should(JoinQueryBuilders.hasChildQuery(
								"airport",
								boolQuery()
								.should(
										JoinQueryBuilders.hasChildQuery(
												"runway",
												matchAllQuery(),
												ScoreMode.Avg)
										.innerHit(new InnerHitBuilder()
												.setSize(ES_MAX_CHILDREN)))
								.should(matchQuery("type", "airport")),
								ScoreMode.Avg)
								.innerHit(new InnerHitBuilder()
										.setSize(ES_MAX_CHILDREN)
										.addSort(SortBuilders.fieldSort("name.keyword"))))
						.should(matchQuery("type", "country")));

		// Execute query against ElasticSearch
		SearchResponse response;
		String filteredResponseJson = "";

		try {
			response = getClient().prepareSearch(ES_INDEX_NAME)
					.setSize(ES_MAX_CHILDREN)
					.addSort("name.keyword", SortOrder.ASC)
					.setQuery(esQuery)
					.execute().actionGet();

			// ElasticSearch response includes a lot of info, so we need to
			// take only the values needed to create the response object
			filteredResponseJson = JsonPath.read(response.toString(), "$.hits.hits[*]").toString();

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		return filteredResponseJson;
	}

	/**
	 * This method implements a query that returns the countries with most airports.
	 * @param size Number of countries to be returned.
	 * @return JSON string with the elasticSearch results of the query.
	 * @throws IOException
	 */
	public String getReportMostAirports(final int size) {
		return getReportAirports(size, SortOrder.DESC);
	}

	/**
	 * This method implements a query that returns the countries with fewer airports.
	 * @param size Number of countries to be returned.
	 * @return JSON string with the elasticSearch results of the query.
	 * @throws IOException
	 */
	public String getReportLowestAirports(final int size) {
		return getReportAirports(size, SortOrder.ASC);
	}

	/**
	 * This method implements a query that returns all countries along with all the
	 * runways associated to each airport.
	 * @return JSON string with the elasticSearch results of the query
	 * @throws IOException
	 */
	public String getReportSurfaces(){

		// This query:
		// Retrieves all countries that match the searchTerm both
		// in the "name" and "code" fields. Along with the country
		// all airports and runways are returned.
		QueryBuilder esQuery = JoinQueryBuilders.hasChildQuery(
				"airport",
				JoinQueryBuilders.hasChildQuery(
						"runway",
						matchAllQuery(),
						ScoreMode.None).innerHit(new InnerHitBuilder()),
				ScoreMode.None).innerHit(new InnerHitBuilder());

		// Execute query against ElasticSearch
		SearchResponse response;
		String filteredResponseJson = "";
		try {
			response = getClient().prepareSearch(ES_INDEX_NAME)
					.setSize(ES_MAX_CHILDREN)
					.addSort("name.keyword", SortOrder.ASC)
					.setQuery(esQuery)
					.execute().actionGet();

			// ElasticSearch response includes a lot of info, so we need to
			// take only the values needed to create the response object.
			filteredResponseJson = JsonPath.read(response.toString(), "$.hits.hits[*]").toString();

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}


		return filteredResponseJson;
	}

	/**
	 * This method implements a query that returns runways le_idents aggregated.
	 * @param size Number of runways le_idents to be returned.
	 * @return JSON string with the elasticSearch results of the query.
	 * @throws IOException
	 */
	public String getReportRunwaysIds(final int size) {

		// This query:
		// Retrieves aggregates all runways le_ident and retrieves the
		// values ordered by doc_count.
		SearchResponse response;
		String filteredResponseJson = "";

		try {
			response = getClient().prepareSearch(ES_INDEX_NAME)
					.setSize(0)
					.addAggregation(AggregationBuilders
							.terms("runwaysids")
							.field("le_ident.keyword")
							.size(size))
					.execute().actionGet();

			filteredResponseJson = JsonPath.read(response.toString(), "$.aggregations[*].buckets[*]").toString();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		// ElasticSearch response includes a lot of info, so we need to
		// take only the values needed to create the response object.


		return filteredResponseJson;
	}

	//
	// Private methods
	//

	/**
	 * This method implements a query that returns countries ordered by the number
	 * of airport children.
	 * @param size Number of Countries to be returned.
	 * @param order SortOrder object. Sort type for the query: ASC/DESC.
	 * @return JSON string with the elasticSearch results of the query.
	 * @throws IOException
	 */
	public String getReportAirports(final int size, final SortOrder order) {

		// This query:
		// Retrieves countries ordered by the number of airport children.
		// The order (ASC/DESC) of the results depends on the parameter "order".
		QueryBuilder esQuery = boolQuery()

				.should(boolQuery()
						.must(matchQuery("_type", "country")).boost(0)
						.mustNot(JoinQueryBuilders.hasChildQuery(
								"airport",
								matchAllQuery(),
								ScoreMode.Total).innerHit(new InnerHitBuilder())))

				.should(JoinQueryBuilders.hasChildQuery(
						"airport",
						matchAllQuery(),
						ScoreMode.Total
						).innerHit(new InnerHitBuilder()));

		SearchResponse response;
		String filteredResponseJson = "";

		try {
			response = getClient().prepareSearch(ES_INDEX_NAME)
					.setSize(size)
					.addSort("_score", order)
					.setQuery(esQuery)
					.execute().actionGet();

			// ElasticSearch response includes a lot of info, so we need to
			// take only the values needed to create the response object
			filteredResponseJson = JsonPath.read(response.toString(), "$.hits.hits[*]").toString();

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}


		return filteredResponseJson;
	}

	/**
	 * This methods returns the TransportClient used to connect with ElasticSearch.
	 * If the client is null a new client is created.
	 * @return TransportClient Object used to connect with elasticSearch.
	 * @throws UnknownHostException
	 */
	@SuppressWarnings("resource")
	private TransportClient getClient() {

		// Only creates a new client if the existing value is null.
		if(client == null) {
			try {
				client = new PreBuiltTransportClient(Settings.EMPTY)
						.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(ES_HOST_NAME), ES_HOST_PORT));
			} catch (UnknownHostException e) {
				System.out.println("Error: " + e.getMessage());
			}
		}

		return client;
	}
}
